import urllib
import cPickle as pickle
import gzip
import os
import sys
import time

import numpy as np
import matplotlib.pyplot as pl 

import theano
import theano.tensor as T

import theano.sandbox.rng_mrg




class Model( object ):
  def __init__( self, xx, yy, winit ):
    self.x = T.matrix( "x" )
    self.y = T.ivector( "y" )
    self.lr = T.scalar( "lr" )
    self.l1 = T.scalar( "l1" )
    self.l2 = T.scalar( "l2" )
    self.momentum = T.scalar( "l2" )
    
    self.xx = xx
    self.yy = yy
    
    self.w = theano.shared( np.asarray( winit ) )
    
    self.p_hat = 1.0 / ( 1.0 + T.exp( -T.dot( self.x, self.w ) ) )
    self.pred = self.p_hat > 0.5
    self.errs = T.neq( self.pred, self.y )
    self.crossEnt = - self.y * T.log( self.p_hat ) - ( 1.0 - self.y ) * T.log( 1.0 - self.p_hat )
  
  
  
  def weights( self ): 
    return np.copy( self.w.get_value( borrow = True ) )
    
    
  def updateFuncs( self ): 
    self.getLCL = theano.function( 
      inputs = [self.x, self.y], 
      outputs = [self.crossEnt] )
      
    self.getErrors = theano.function( 
      inputs = [self.x, self.y], 
      outputs = [self.errs] )
      
    self.getProbability = theano.function( 
      inputs = [self.x], 
      outputs = [self.p_hat], 
      on_unused_input = "ignore" )
      
    
    
    
  def normalFuncs( self ):
    self.cost = \
      self.crossEnt.sum() + \
      self.l1 * T.sum( abs( self.w[1:] ) ) + \
      self.l2 * T.mean( self.w[1:] ** 2.0 ) 
      
    self.gradient = T.grad( self.cost, self.w )
    
    self.train = theano.function( 
      inputs = [self.x, self.y, self.lr, self.l1, self.l2], 
      outputs = [self.p_hat], 
      updates = ( ( self.w, self.w - self.lr * self.gradient ), ) )
    
    self.updateFuncs()
    
    return self.getLCL, self.getErrors, self.getProbability, self.train
      
    
    
    
  def momentuFuncs( self ):
    self.cost = \
      self.crossEnt.mean() + \
      self.l1 * T.sum( abs( self.w[1:] ) ) + \
      self.l2 * T.mean( self.w[1:] ** 2.0 ) 
      
    self.gradient = T.grad( self.cost, self.w )
      
    self.train = theano.function( 
      inputs = [self.x, self.y, self.lr, self.l1, self.l2], 
      outputs = [self.p_hat], 
      updates = ( ( self.w, self.w - self.lr * self.gradient ), ) )
    
    return self.getLCL, self.getErrors, self.getProbability, self.train



if __name__ == "__main__":
  import LogisticRegression
