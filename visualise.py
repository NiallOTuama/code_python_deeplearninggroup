import urllib
import cPickle as pickle
import gzip
import os
import sys
import time

import numpy as np
import matplotlib.pyplot as pl 

import theano
import theano.tensor as T

import theano.sandbox.rng_mrg

from common import *

from matplotlib import cm
import matplotlib.pyplot as pl   
from mpl_toolkits.mplot3d import axes3d
from mpl_toolkits.axes_grid1 import make_axes_locatable


def plotDecisionBoundary( model, axes, xes, yes, minimax, plot3d = False, n = 100, stride = 2 ):
  xmin = np.floor( np.min( ( xes[:,1].min(), minimax.min() ) ) )
  xmax = np.ceil( np.max( ( xes[:,1].max(), minimax.max() ) ) )
  
  ymin = np.floor( np.min( ( xes[:,2].min(), minimax.min() ) ) )
  ymax = np.ceil( np.max( ( xes[:,2].max(), minimax.max() ) ) )
  
  plotrange_x = np.linspace( xmin, xmax, n )
  plotrange_y = np.linspace( ymin, ymax, n )
  mgx, mgy = np.meshgrid( plotrange_x, plotrange_y )
  xxx = np.asarray( np.c_[np.ones( n * n ), mgx.ravel(), mgy.ravel() ], dtype = np.float32 )
  
  
  mgz = np.asarray( model.getProbability( xxx ) ).reshape( mgx.shape )
  probs = np.asarray( model.getProbability( np.asarray( xes, dtype = np.float32 ) ) ).reshape( -1 )

  w = model.weights()
  
  
  if plot3d:
    fig = pl.figure()
    ax = fig.gca( projection = '3d' )
    
    surf = ax.plot_surface( 
      mgx, 
      mgy, 
      mgz,
      alpha = 0.25,
      rstride = stride,
      cstride = stride,
      cmap = cm.coolwarm,
      linewidth = 0, 
      antialiased = False )
    ax.plot_surface( 
      mgx,
      mgy,
      np.ones( mgx.shape ) * 0.5,
      alpha = 0.1,
      rstride = stride,
      cstride = stride,
      cmap = cm.coolwarm,
      linewidth = 0,
      antialiased = False )
      
    yp = yes == 1
    yn = yes == 0
    ax.scatter( xes[yn, 1], xes[yn, 2], probs[yn], c = "b", s = 30 )
    ax.scatter( xes[yp, 1], xes[yp, 2], probs[yp], c = "r", s = 30 )
    
    #ax.scatter( xes[yn, 1], xes[yn, 2], 0.0 * np.ones( np.count_nonzero( yn ) ), c = "b", s = 30 )
    #ax.scatter( xes[yp, 1], xes[yp, 2], 1.0 * np.ones( np.count_nonzero( yp ) ), c = "r", s = 30 )
    ax.set_zlim(  0.0, 1.0 )
    
  ax = pl.gca()
  
  pl.pcolor( 
    mgx,
    mgy,
    mgz,  #mgz * ( 1.0 - mgz ),
    alpha = 0.5,
    cmap = cm.coolwarm )
  #pl.pcolor( 
  #  mgx,
  #  mgy,
  #  mgz > 0.5,
  #  alpha = 0.05,
  #  cmap = cm.coolwarm )
  
  wscale = -w[0] / sum( w[1:] ** 2 )
  wplot = w[1:] * wscale
  pl.scatter( xes[yes == 0, 1], xes[yes == 0, 2], c = "b", s = 30 )
  pl.scatter( xes[yes == 1, 1], xes[yes == 1, 2], c = "r", s = 30 )
  pl.plot( [0.0, w[1]], [0.0, w[2]], "--k" )
  ax.arrow( 0.0, 0.0, wplot[0], wplot[1], head_width = 0.05, head_length = 0.1, fc = "k", ec = "k" )
  ax.set_aspect( 1 )
  ax.grid()
    

  ax.set_xlim( xmin, xmax )
  ax.set_ylim( ymin, ymax )





def plotErrorSurface( model, ax, xes, yes, minimax, n = 100 ): 
  ww = model.weights()
  
  xmin = np.floor( np.min( ( xes[:,1].min(), minimax.min() ) ) )
  xmax = np.ceil( np.max( ( xes[:,1].max(), minimax.max() ) ) )
  
  ymin = np.floor( np.min( ( xes[:,2].min(), minimax.min() ) ) )
  ymax = np.ceil( np.max( ( xes[:,2].max(), minimax.max() ) ) )
  
  plotrange_x = np.linspace( xmin, xmax, n )
  plotrange_y = np.linspace( ymin, ymax, n )
  
  w1, w2 = np.meshgrid( plotrange_x, plotrange_y )
  
  #ws = np.asarray( np.c_[ w1.ravel() * w2.ravel(), w1.ravel(), w2.ravel() ], dtype = np.float32 )
  ws = np.asarray( np.c_[ ww[0] * np.ones( n * n ), w1.ravel(), w2.ravel() ], dtype = np.float32 )
  zz = np.zeros( len( ws ) )
  
  for wi, wv in enumerate( ws ): 
    #wscale = -wv[0] / sum( wv[1:] ** 2 )
    #wv[1:] = wv[1:] * wscale
    
    model.w.set_value( wv )
    v = np.mean( model.getLCL( xes, yes )[0] )
    zz[wi] = v
  zz = zz.reshape( w1.shape )
  
  pl.sca( ax )
  
  y, x = np.unravel_index( zz.argmin(), zz.shape )
  pl.scatter( [plotrange_x[x]], [plotrange_y[y]], c = "k", s = 50 )
  
  pl.pcolor( w1, w2, np.log( zz ), alpha = 0.25 )
  ax.grid()
  ax.set_aspect( 1 )
  
  ax.set_xlim( plotrange_x[0], plotrange_x[-1] )
  ax.set_ylim( plotrange_y[0], plotrange_y[-1] )





def plotErrorSurface2( model, ax, xes, yes, minimax, n ): 
  ww = model.weights()
  b = ww[0]
  
  xmin = np.floor( np.min( ( xes[:, 1].min(), minimax.min() ) ) )
  xmax = np.ceil( np.max( ( xes[:, 1].max(), minimax.max() ) ) )
  
  ymin = np.floor( np.min( ( xes[:, 2].min(), minimax.min() ) ) )
  ymax = np.ceil( np.max( ( xes[:, 2].max(), minimax.max() ) ) )
  
  plotrange_x = np.linspace( xmin, xmax, n )
  plotrange_y = np.linspace( ymin, ymax, n )
  
  xx, yy = np.meshgrid( plotrange_x, plotrange_y )
  
  ws = np.asarray( np.c_[ b * np.ones( n * n ), xx.ravel(), yy.ravel() ], dtype = np.float32 )
  
  fig = pl.figure()
  ax = fig.gca( projection = '3d' )
  
  
  
  for b in np.linspace( -10.0, 10.0, 10 ):
    zz = np.zeros( len( ws ) )
    
    for wi, wv in enumerate( ws ):
      wv[0] = b
      model.w.set_value( np.copy( wv ) )
      zz[wi] = np.mean( model.getLCL( xes, yes )[0] )
      
    zz = zz.reshape( xx.shape )
    
    surf = ax.plot_surface( 
      xx,
      yy,
      b * np.ones( xx.shape ),
      facecolors = cm.jet( np.log( zz ) ),
      linewidth = 0,
      antialiased = False,
      rstride = 5,
      cstride = 5,
      alpha = 0.1 )
  


if __name__ == "__main__":
  import LogisticRegression
