import urllib
import cPickle as pickle
import gzip
import os
import sys
import time

import numpy as np
import matplotlib.pyplot as pl 

import theano
import theano.tensor as T

import theano.sandbox.rng_mrg
TheanoRandomStreams = theano.sandbox.rng_mrg.MRG_RandomStreams




def sharedDataset( x, y ):
  xx = theano.shared( np.asarray( x, dtype = theano.config.floatX ), borrow = True )
  yy = theano.shared( np.asarray( y, dtype = theano.config.floatX ), borrow = True )

  return xx, T.cast( yy, 'int32' )
  
  
  
def randomData( labels, numExamples, ndims, means, covariances, rng ):
  xes = []
  yes = []
  
  numExamples /= 2
  for y, mu, sigma in zip( labels, means, covariances ): 
    x = rng.multivariate_normal( mu, sigma, numExamples )
    x = np.column_stack( ( np.ones( numExamples ), x ) ) 
      
    xes.append( x )
    yes.append( np.ones( numExamples ) * y )
  
  xes = np.asarray( np.concatenate( xes ), dtype = np.float32 )
  yes = np.asarray( np.concatenate( yes ), dtype = np.int )
  
  perm = rng.permutation( len( yes ) )
  xes = xes[perm]
  yes = yes[perm]
  
  return xes, yes, sharedDataset( xes, yes )



if __name__ == "__main__":  
  randomData( 
    1000, 
    2, 
    ( [-1.0,-1.0], [1.0, 1.0] ), 
    ( [0.75, 0.75], [0.75, 0.75] ) )
