import urllib
import cPickle as pickle
import gzip
import os
import sys
import time

import numpy as np
import matplotlib.pyplot as pl 

import theano
import theano.tensor as T

import theano.sandbox.rng_mrg

from common import *
from visualise import *


from Model import Model





randomSeed = 1234

numFeatures = 2
numExamples = 200
batchSize = 10
numInitPositions = 1
numLearningIterations = 100

numDemoRows = 3


labels = [0, 1]

means = ( np.asarray( [+1.0, +1.0] ) + 2,
          np.asarray( [-1.0, -1.0] ) + 2 )
        
cov1 = np.asarray( [[1.0, 0.2],
                    [0.4, 1.0]] )
                    
cov2 = np.asarray( [[1.0, -0.2],
                    [-0.4, 1.0]] )
                   
covariances = np.asarray( ( cov1, cov2 ) )

winit = np.asarray( [0.0, -2.0, 2.0], dtype = np.float32 )

plotPrecision = 200

lr, l1, l2 = 0.3, 0e-1, 0e-1




#rng = np.random
rng = np.random.RandomState( randomSeed )

xes, yes, ( xx, yy ) = randomData( 
  labels, 
  numExamples, 
  numFeatures, 
  means, 
  covariances, 
  rng )





model = Model( xx, yy, winit )
getLCL, getErrors, getProbability, train = \
  model.normalFuncs()




minimax_plt = np.asarray( [0.0, 0.0], dtype = np.float32 )
minimax_err = np.asarray( [-10, 10], dtype = np.float32 )



# Learn, and plot trajectory through weight space
for i in np.arange( numInitPositions ):
  fig, axes = pl.subplots( 1, 2 )
  pl.sca( axes[0] )

  init = np.asarray( np.concatenate( ( [0], rng.uniform( minimax_err.min(), minimax_err.max(), 2 ) ) ), dtype = np.float32 )
  model.w.set_value( np.asarray( winit if i == 0 else init, dtype = np.float32 ) )
  
  
  print "Initialised Weights:", model.weights()
  
  for _ in np.arange( numLearningIterations ): 
    whichExamples = np.array_split( rng.permutation( len( yes ) ), numExamples / batchSize )
    
    for exampleRange in whichExamples:
      previousWeights = model.weights()
      train( xes[exampleRange], yes[exampleRange], lr, l1, l2 )
      currentWeights = model.weights()
      
      #pl.plot( [previousWeights[1], currentWeights[1]],
      #         [previousWeights[2], currentWeights[2]], "k" )
  
  print "Learnt Weights:     ", model.weights()
  print "Error: %.3f%%" % ( np.mean( getErrors( xes, yes ) ) * 100 )
  print "LCL:   %.3f"   % ( np.sum( getLCL( xes, yes ) ) )
  print
  
  pl.sca( axes[0] )
  plotDecisionBoundary( model, axes, xes, yes, minimax = minimax_plt, plot3d = 0, n = plotPrecision, stride = 5 )
  
  pl.sca( axes[1] )
  w = model.weights()
  w[:] /= 2
  model.w.set_value( w )
  plotDecisionBoundary( model, axes, xes, yes, minimax = minimax_plt, plot3d = 0, n = plotPrecision, stride = 5 )
  #plotErrorSurface( model, axes[0], xes, yes, minimax = minimax_err, n = plotPrecision )

  fig.tight_layout()



"""
# Demonstrate different error surfaces for different mini-batches 
fig, axes = pl.subplots( numDemoRows, numDemoRows )
axes = axes.reshape( -1 )
whichExamples = np.array_split( rng.permutation( len( yes ) ), numExamples / batchSize )

for ei in np.arange( numDemoRows * numDemoRows ):
  exampleRange = rng.randint( low = 0, high = len( yes ), size = batchSize )
  plotErrorSurface( model, axes[ei], xes[exampleRange], yes[exampleRange], minimax = minimax_err, n = 100 )

fig.tight_layout()
"""



pl.show()

